# CHANGELOG

This project adheres to [Semantic Versioning](http://semver.org/).
Which is based on [Keep A Changelog](http://keepachangelog.com/)

## Unreleased

### Added

- test: add support debian 12
- feat: manage logging config

### Changed

- test: use personal docker registry

### Removed

- test: remove support debian 10
- test: remove support debian 11

## v1.1.0 - 2021-08-24

### Added

- feat: add logstash patterns for grok
- test: add support debian 11

### Changed

- change logstash_inputs variable: dict to array
- test: replace kitchen to molecule
- chore: use FQCN for module name

### Removed

- test: remove support debian 9

## v1.0.0 - 2019-09-29

### Added

- copy configuration
- use the logstash config test to validate the configuration
