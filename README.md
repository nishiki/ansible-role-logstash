# Ansible role: Logstash

[![Version](https://img.shields.io/badge/latest_version-1.1.0-green.svg)](https://code.waks.be/nishiki/ansible-role-logstash/releases)
[![License](https://img.shields.io/badge/license-Apache--2.0-blue.svg)](https://code.waks.be/nishiki/ansible-role-logstash/src/branch/main/LICENSE)
[![Build](https://code.waks.be/nishiki/ansible-role-logstash/actions/workflows/molecule.yml/badge.svg?branch=main)](https://code.waks.be/nishiki/ansible-role-logstash/actions?workflow=molecule.yml)

Install and configure logstash

## Requirements

- Ansible >= 2.9
- Debian
  - Bookworm

## Role variables

- `logstash_major_version` set major version to install- (default: `7`)
- `logstash_plugins` - array with the plugins to install

```
  - name: logstash-output-influxdb
    state: present
```

- `logstash_config` - hash with the configuration (see [logstash documentation](https://www.elastic.co/guide/en/logstash/current/configuration.html))

```
  path.data: /var/lib/logstash
  path.logs: /var/log/logstash
```

- `logstash_inputs` - hash with the inputs configurations

```
  file:
    path: /var/log/syslog
  beats:
    port: 5444
```

- `logstash_outputs` - array with the outputs configuration

```
  - >
    file {
      path => "/var/log/syslog"
      codec => "json"
    }
```

- `logstash_filters` - array with the filters configuration

```
  - >
    grok {
      match => { "message" => "%{IP:client} %{WORD:method} %{URIPATHPARAM:request} %{NUMBER:bytes} %{NUMBER:duration}" }
    }
```

- `logstash_pattern` - hash with grok patterns

```
  postfix: |
    # Syslog stuff
    PROCESS ([\w._\/%-]+)
    COMPID postfix\/%{PROCESS:process}(?:\[%{NUMBER:pid}\])?
    POSTFIX (?:%{SYSLOGTIMESTAMP:timestamp}|%{TIMESTAMP_ISO8601:timestamp8601}) (?:%{SYSLOGFACILITY} )?%{SYSLOGHOST:logsource} %{COMPID}:
```

- `logstash_logging_config` - hash with logging config (log4j2)

```yaml
status: "error"
name: "LogstashPropertiesConfig"
appender.rolling.type: "RollingFile"
appender.rolling.name: "plain_rolling"
appender.rolling.fileName: "${sys:ls.logs}/logstash-plain.log"
appender.rolling.filePattern: "${sys:ls.logs}/logstash-plain-%d{yyyy-MM-dd}-%i.log.gz"
appender.rolling.policies.type: "Policies"
appender.rolling.policies.time.type: "TimeBasedTriggeringPolicy"
appender.rolling.policies.time.interval: "1"
appender.rolling.policies.time.modulate: true
appender.rolling.layout.type: "PatternLayout"
appender.rolling.layout.pattern: "[%d{ISO8601}][%-5p][%-25c]%notEmpty{[%X{pipeline.id}]}%notEmpty{[%X{plugin.id}]} %m%n"
appender.rolling.policies.size.type: "SizeBasedTriggeringPolicy"
appender.rolling.policies.size.size: "100MB"
appender.rolling.strategy.type: "DefaultRolloverStrategy"
appender.rolling.strategy.max: 30
appender.rolling.avoid_pipelined_filter.type: "PipelineRoutingFilter"
appender.routing.type: "PipelineRouting"
appender.routing.name: "pipeline_routing_appender"
appender.routing.pipeline.type: "RollingFile"
appender.routing.pipeline.name: "appender-${ctx:pipeline.id}"
appender.routing.pipeline.fileName: "${sys:ls.logs}/pipeline_${ctx:pipeline.id}.log"
appender.routing.pipeline.filePattern: "${sys:ls.logs}/pipeline_${ctx:pipeline.id}.%i.log.gz"
appender.routing.pipeline.layout.type: "PatternLayout"
appender.routing.pipeline.layout.pattern: "[%d{ISO8601}][%-5p][%-25c] %m%n"
appender.routing.pipeline.policy.type: "SizeBasedTriggeringPolicy"
appender.routing.pipeline.policy.size: "100MB"
appender.routing.pipeline.strategy.type: "DefaultRolloverStrategy"
appender.routing.pipeline.strategy.max: 30
rootLogger.level: "${sys:ls.log.level}"
rootLogger.appenderRef.rolling.ref: "${sys:ls.log.format}_rolling"
rootLogger.appenderRef.routing.ref: "pipeline_routing_appender"
```

## How to use

```
- hosts: server
  roles:
    - logstash
```

## Development

### Test with molecule and docker

- install [docker](https://docs.docker.com/engine/installation/)
- install `python3` and `python3-pip`
- install molecule and dependencies `pip3 install molecule molecule-docker docker ansible-lint pytest-testinfra yamllint`
- run `molecule test`

## License

```
Copyright (c) 2019 Adrien Waksberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
