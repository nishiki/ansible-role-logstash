import testinfra.utils.ansible_runner
import time

def test_packages(host):
  package = host.package('logstash')
  assert package.is_installed

def test_config_file(host):
  path = host.file('/etc/logstash/logstash.yml')
  assert path.exists
  assert path.is_file
  assert path.user == 'root'
  assert path.group == 'root'
  assert path.mode == 0o644

def test_logging_config_file(host):
  path = host.file('/etc/logstash/log4j2.properties')
  assert path.exists
  assert path.is_file
  assert path.user == 'root'
  assert path.group == 'root'
  assert path.mode == 0o644
  assert path.contains('appender.rolling.strategy.max = 15')

def test_input_file(host):
  path = host.file('/etc/logstash/conf.d/input.conf')
  assert path.exists
  assert path.is_file
  assert path.user == 'root'
  assert path.group == 'root'
  assert path.mode == 0o644
  assert path.contains('path => "/var/log/syslog"')

def test_patterns_file(host):
  path = host.file('/etc/logstash/patterns/postfix.conf')
  assert path.exists
  assert path.is_file
  assert path.user == 'root'
  assert path.group == 'root'
  assert path.mode == 0o644
  assert path.contains('# Syslog stuff')

def test_service(host):
  service = host.service('logstash')
  assert service.is_running
  assert service.is_enabled

def test_output(host):
  time.sleep(20)
  host.run('echo test >> /var/log/syslog')
  time.sleep(20)
  path = host.file('/tmp/logstash.log')
  assert path.exists
  assert path.is_file
  assert path.contains('test')

def test_socket(host):
  socket = host.socket('tcp://127.0.0.1:9600')
  assert socket.is_listening
